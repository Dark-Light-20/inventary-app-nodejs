const mysql = require('mysql');

const startConnection = () => {
  // Start connection
  const connection = mysql.createConnection({
    host: 'localhost',
    user: 'node',
    password: 'node123',
    database: 'node'
  });
  // Validate conncetion
  connection.connect((errC) => {
    if (errC) throw errC;
    console.log('Connection established...');
  });
  return connection;
};

const endConnection = (connection) => {
  // Close connection
  connection.end((errE) => {
    if (errE) throw errE;
    console.log('Connection finished...');
  });
};

module.exports.startConnection = startConnection;
module.exports.endConnection = endConnection;
