// Requires
const express = require('express');
const bodyParser = require('body-parser');
const path = require('path');
const { startConnection, endConnection } = require('./modules/bdFunctions');

// Server instance
const app = express();

// Load utilities
app.use(express.static(path.join(__dirname, './public')));
app.set('views', 'views');
app.set('view engine', 'pug');

// Here we are configuring express to use body-parser as middle-ware.
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

// Routes
app.get('/', (req, res) => {
  res.render('index');
});

app.get('/registro', (req, res) => {
  res.render('registro');
});

app.post('/nuevo', (req, res) => {
  const { ID, name } = req.body;
  let { quantity } = req.body;
  quantity = Number.parseInt(quantity, 10);
  if (ID.length === 3) {
    // Start connection
    const connection = startConnection();
    // INSERT on items table
    connection.query(`INSERT INTO items values("${ID}","${name}")`, (err) => {
      if (err) {
        throw err;
      } else {
        console.log(`Inserted on items: ${ID} -> ${name}`);
        // INSERT on quantities table
        connection.query(
          `INSERT INTO quantities values("${ID}",${quantity})`,
          (err2) => {
            if (err2) throw err2;
            console.log(`Inserted on quantities: ${ID} -> ${quantity}`);
          }
        );
        // Close connection
        endConnection(connection);
      }
    });
    res.render('nuevo', { ID, name, quantity });
  }
});

app.get('/consulta', (req, res) => {
  // Start connection
  const connection = startConnection();
  // SELECT query
  connection.query(
    'SELECT ID, name, quantity FROM items NATURAL JOIN quantities',
    (err, results) => {
      if (err) throw err;
      else {
        const items = [];
        Object.keys(results).forEach((element) => {
          items.push([
            results[element].ID,
            results[element].name,
            results[element].quantity
          ]);
        });
        res.render('consulta', { items });
      }
    }
  );
  // Close connection
  endConnection(connection);
});

app.get('/modificar/:id', (req, res) => {
  const { id } = req.params;
  const title = `Modificar item ${id}`;
  res.render('modificar', { title, id });
});

app.post('/modificar/:id/action', (req, res) => {
  // Vars
  const { id } = req.params;
  const { op } = req.body;
  let { num } = req.body;
  num = Number.parseInt(num, 10);
  // Start connection
  const connection = startConnection();
  // SELECT query
  connection.query(
    `SELECT quantity FROM quantities where ID="${id}"`,
    (err, result) => {
      if (err) throw err;
      else {
        let { quantity } = result[0];
        // Sale
        if (op === '2' && quantity - num >= 0) quantity -= num;
        else if (op === '2' && quantity - num < 0) {
          res.render('update', {
            title: 'Modificación fallida',
            h1: 'La cantidad a vender es mayor a la disponible en inventario'
          });
          // Acquire
        } else if (op === '1') quantity += num;
        // Op error
        else {
          res.status(500).render('update', {
            title: 'Modificación fallida',
            h1: 'Error de modificación... Opción incorrecta'
          });
        }
        // UPDATE query Sale
        connection.query(
          `UPDATE quantities SET quantity=${quantity} where ID="${id}"`,
          (err2) => {
            if (err2) throw err2;
            else {
              console.log('Quantity for ID: ', id, ' => ', quantity);
              res.render('update', {
                title: 'Modificación exitosa',
                h1: 'Actualización de unidades en inventario correcta'
              });
            }
          }
        );
        // Close connection
        endConnection(connection);
      }
    }
  );
});

app.get('/eliminar/:id', (req, res) => {
  const { id } = req.params;
  const title = `Eliminar item ${id}`;
  res.render('eliminar', { title, id });
});

app.post('/eliminar/:id/action', (req, res) => {
  // Vars
  const { id } = req.params;
  // Start connection
  const connection = startConnection();
  // DELETE query on quantites
  connection.query(`DELETE from quantities where ID="${id}"`, (err) => {
    if (err) throw err;
    else {
      // DELETE query on items
      connection.query(`DELETE from items where ID="${id}"`, (err2) => {
        if (err2) throw err2;
        else {
          console.log('Delete ID: ', id);
          res.render('update', {
            title: 'Elimicación exitosa',
            h1: 'Se ha eliminado el producto del inventario'
          });
        }
      });
      // Close connection
      endConnection(connection);
    }
  });
});

app.listen(4000, () => {
  console.log('Running on port 4000');
});
